/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class PlayerService {
    static Player o, x;

    static {
        o = new Player('O');
        x = new Player('X');
    }

    public static void load() {
        File f = null;
        FileInputStream fileInput = null;
        ObjectInputStream obInput = null;
        try {
            f = new File("ox.bin");
            fileInput = new FileInputStream(f);
            obInput = new ObjectInputStream(fileInput);
            o = (Player) obInput.readObject();
            x = (Player) obInput.readObject();
            System.out.println(o);
            System.out.println(x);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }
    }

    public static void save() {
        File f = null;
        FileOutputStream fileOutput = null;
        ObjectOutputStream obOutput = null;
        try {
            f = new File("ox.bin");
            fileOutput = new FileOutputStream(f);
            obOutput = new ObjectOutputStream(fileOutput);
            obOutput.writeObject(o);
            obOutput.writeObject(x);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                if (fileOutput != null) {
                    fileOutput.close();
                }
                if (obOutput != null) {
                    obOutput.close();
                }
            } catch (IOException ex) {
            }
        }
    }

    public static Player getO() {
        return o;
    }

    public static Player getX() {
        return x;
    }

}
