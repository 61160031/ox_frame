/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class TestReadFile {
    public static void main(String[] args) {
        Player o, x;
        File f = null;
        FileInputStream fileInput = null;
        ObjectInputStream obInput = null;
        try {
            f = new File("ox.bin");
            fileInput = new FileInputStream(f);
            obInput = new ObjectInputStream(fileInput);
            o = (Player) obInput.readObject();
            x = (Player) obInput.readObject();
            System.out.println(o);
            System.out.println(x);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }
        
    }

}
