/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class TestWriteFile {
    public static void main(String[] args) {
        File f = null;
        FileOutputStream fileOutput = null;
        ObjectOutputStream obOutput = null;
        Player o = new Player('O');
        Player x = new Player('X');
        o.draw();
        x.draw();
        o.win();
        x.lose();
        o.win();
        x.lose();
        try {
            f = new File("ox.bin");
            fileOutput = new FileOutputStream(f);
            obOutput = new ObjectOutputStream(fileOutput);
            obOutput.writeObject(o);
            obOutput.writeObject(x);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                if (fileOutput != null) {
                    fileOutput.close();
                }
                if (obOutput != null) {
                    obOutput.close();
                }
            } catch (IOException ex) {
            }
        }


    }

}
